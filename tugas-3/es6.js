//No 1. Mengubah Fungsi menjadi fungsi arrow
const golden = () => { console.log("this is golden!!"); }
golden();

//No 2. Sederhanakan menjadi objek literal di ES6
const functionLiteral = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullname: () => {
            console.log(firstName + " " + lastName);
            return
        }
    }
}
functionLiteral("wiliam", "imoh").fullname()

//No 3. Distructuring
const newobjek = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"

}
const { firstName, lastName, destination, occupation, spell } = newobjek;
console.log(newobjek);

//No 4. Array speading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
console.log(combined);

//no 5. Template literals
const planet = "earth"
const view = "glass"
var before = `Lorem ` + view + `dolor sit amet, ` +
    `consectetur adipiscing elit` + planet + `do eiusmod tempor ` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`
console.log(before)
console.log("Looping Pertama")
var naik = 2;
while(naik <=20) { 
 console.log(naik + ' - I Love Coding');
 naik+=2; 
}
console.log("")

console.log("Looping Kedua")
var turun = 20;
while(turun >=2) { 
 console.log(turun + ' - I Will Become A Mobile Developer');
 turun-=2; 
}
console.log("")


console.log("Looping Mengguanakan For")
for(var angka = 1; angka <= 20; angka++) {

    if (angka % 2 == 1 && angka % 3 == 0){
        console.log(angka + ' - I Love Coding')    
    } else if (angka % 2 == 1){
        console.log(angka + ' - Teknik')
    } 
       else{
    console.log(angka + ' - Informatika');
    }    
   }
console.log('');


console.log('Membuat Persegi Panjang');
   class Persegi {
       run(n,m){
           for (var i=1; i<=n; i++){
               var x='';
               for (var j=1; j<=m; j++){
                   var x= x + '#';
               }
               console.log(x);
           }
        }
   }
   var persegi = new Persegi;
 persegi.run(4,8);
 console.log('')
 

 console.log('Membuat Tangga');
   class Tangga {
       run(n){
           for (var i=1; i<=n; i++){
               var x='';
               for (var j=1; j<=i; j++){
                   var x= x + '#';
               }
               console.log(x);
           }
        }
   }
var tangga = new Tangga;
tangga.run(7);
console.log('')
 


console.log('Membuat Papan Catur');
class Catur {
    run(n,m){
        for (var i=1; i<=n; i++){
            if(i%2==0){
             var x=' ';
            } else{
                var x='';
            }
            for (var j=1; j<=m; j++){
                if(j%2==0){
                 var x= x +' ';    
                } else {
                var x= x + '#';
                }
            }
            console.log(x);
        }
     }
}
var catur = new Catur;
catur.run(8,8);


  